# libreoffice-impress

Presentation component for LibreOffice office productivity suite. https://libreoffice.org/discover/impress/

## Official documentation
* [*How can I get bullet points to display separately instead of seeing the whole slide?*
  ](https://ask.libreoffice.org/en/question/94035/how-can-i-get-bullet-points-to-display-separately-instead-of-seeing-the-whole-slide/)
  2017-05 cwhitty317, Regina